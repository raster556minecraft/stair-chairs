package com.rast.stairchairs;

import org.bukkit.*;
import org.bukkit.attribute.Attribute;
import org.bukkit.attribute.AttributeInstance;
import org.bukkit.block.Block;
import org.bukkit.block.data.Bisected;
import org.bukkit.block.data.Directional;
import org.bukkit.block.data.type.Stairs;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.spigotmc.event.entity.EntityDismountEvent;

import java.util.HashSet;

public class EventListener implements Listener {

    private final StairChairs plugin;
    private final PlayerManager playerManager;
    private final HashSet<Material> blocks = new HashSet<>();
    private final Double sitOffsetX;
    private final Double sitOffsetY;
    private final Double sitOffsetZ;
    private final Double sitDistance;

    public EventListener (StairChairs plugin) {
        // Get the plugin and config
        this.plugin = plugin;
        FileConfiguration config = plugin.getConfig();

        // Get player manager
        playerManager = plugin.getPlayerManager();

        // Load sit offset
        sitOffsetX = config.getDouble("sit-offset.X", 0.5);
        sitOffsetY = config.getDouble("sit-offset.Y", -1.2);
        sitOffsetZ = config.getDouble("sit-offset.Z", 0.5);

        // Load sit distance
        sitDistance = config.getDouble("sit-distance", 1.8);

        // Load blocks
        for (String matString : config.getStringList("blocks")) {
            blocks.add(Material.valueOf(matString));
        }
    }

    @EventHandler
    public void onEntityDismount (EntityDismountEvent e) {
        // See if it is a player and is mounting an armor stand
        if (e.getEntityType().equals(EntityType.PLAYER)){
            Player player = (Player) e.getEntity();
            ArmorStand armorStand = playerManager.getPlayerArmorStandPair(player);
            if (armorStand != null) {
                // Unclaim block
                playerManager.removeUsedBlock(player);
                armorStand.remove();
                playerManager.removePlayerArmorStandPair(player);
                playerManager.removePlayerLegsLocked(player);
                Bukkit.getScheduler().runTask(plugin, () -> player.teleport(playerManager.removeLastLocation(player)));
            }
        }
    }

    @EventHandler
    public void onPlayerJoin (PlayerJoinEvent e) {
        // Store the player sit mode in cache from storage
        plugin.loadPlayerStorage();
        playerManager.setPlayerSitMode(e.getPlayer(), plugin.getPlayerStorage().getBoolean(e.getPlayer().getUniqueId().toString(), true));
    }

    @EventHandler
    public void onPlayerQuit (PlayerQuitEvent e) {

        Player player = e.getPlayer();

        // Remove the armor stand
        ArmorStand armorStand = playerManager.getPlayerArmorStandPair(player);
        if (armorStand != null) {
            // Unclaim block
            playerManager.removeUsedBlock(player);
            armorStand.remove();
        }
        // Remove player from player manager because they are gone from the server
        playerManager.removePlayerArmorStandPair(player);
        playerManager.removeLastLocation(player);
        playerManager.removePlayerSitMode(player);
        playerManager.removePlayerLegsLocked(player);
        // Save the player sit mode from cache to storage
        plugin.getPlayerStorage().set(player.getUniqueId().toString(), playerManager.getPlayerSitMode(player));
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent e) {

        Player player = e.getPlayer();

        // We don't want the player to sit if they are out of sit mode
        if (!playerManager.getPlayerSitMode(player) || player.getVehicle() != null)
            return;

        // Only check if we are right clicking and it is our main hand and our hand is empty
        if (e.getAction().equals(Action.RIGHT_CLICK_BLOCK) && e.getHand() == EquipmentSlot.HAND && e.getItem() == null) {
            // Make sure our block is not null
            if (e.getClickedBlock() == null)
                return;

            Block clickedBlock = e.getClickedBlock();

            // Make sure player is not too far
            if (player.getLocation().distance(clickedBlock.getLocation().add(0.5, 0.5, 0.5)) > sitDistance)
                return;

            // Make sure that the block is not already occupied
            if (playerManager.getUsedBlocks().contains(clickedBlock)) {
                player.sendMessage(ChatColor.RED + "This chair is occupied!");
                return;
            }

            // Make sure this stair is not upside down
            if (clickedBlock.getBlockData() instanceof Bisected) {
                Bisected bisected = (Bisected) clickedBlock.getBlockData();
                if (bisected.getHalf() == Bisected.Half.TOP)
                    return;
            }

            // See if clicked block is a stair
            if (blocks.contains(clickedBlock.getType())) {
                Location location = clickedBlock.getLocation().add(sitOffsetX, sitOffsetY, sitOffsetZ);
                // Modify location if it is a stair
                if (clickedBlock.getBlockData() instanceof Stairs) {
                    Directional directional = (Directional) clickedBlock.getBlockData();
                    location.setDirection(directional.getFacing().getDirection().multiply(-1));
                    Stairs stair = (Stairs) clickedBlock.getBlockData();
                    stair.getShape();
                    switch (stair.getShape()) {
                        case INNER_LEFT:
                        case OUTER_LEFT:
                            location.setYaw(location.getYaw() - 45);
                            break;
                        case INNER_RIGHT:
                        case OUTER_RIGHT:
                            location.setYaw(location.getYaw() + 45);
                            break;
                    }
                    playerManager.setPlayerLegsLocked(player, true);
                } else {
                    playerManager.setPlayerLegsLocked(player, false);
                }
                // Make our armor stand
                ArmorStand armorStand = (ArmorStand) clickedBlock.getWorld().spawnEntity(location, EntityType.ARMOR_STAND);
                armorStand.setVisible(false);
                armorStand.setGravity(false);
                armorStand.setInvulnerable(true);
                armorStand.setPersistent(false);
                AttributeInstance attribute = armorStand.getAttribute(Attribute.GENERIC_MAX_HEALTH);
                assert attribute != null;
                attribute.setBaseValue(1);
                // Store last location
                playerManager.addLastLocation(player, player.getLocation());
                // Make the player a passenger
                armorStand.addPassenger(player);
                // Store the player and the armor stand together
                playerManager.setPlayerArmorStandPair(player, armorStand);
                // Claim the block as used
                playerManager.addUsedBlock(player, clickedBlock);
                e.setCancelled(true);
            }
        }
    }
}
