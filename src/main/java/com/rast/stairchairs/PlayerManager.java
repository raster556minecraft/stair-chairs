package com.rast.stairchairs;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class PlayerManager {

    private StairChairs plugin;
    private HashMap<Player, ArmorStand> playerArmorStandPair = new HashMap<>();
    private HashSet<String> removedStandUUIDs = new HashSet<>();
    private HashMap<Player, Boolean> playerLegsLocked = new HashMap<>();
    private HashMap<Player, Boolean> playerSitMode = new HashMap<>();
    private HashMap<Player, Location> lastLocation = new HashMap<>();
    private HashMap<Player, Block> usedBlocks = new HashMap<>();


    public PlayerManager(StairChairs plugin) {
        this.plugin = plugin;
    }

    // Toggle weather the player can sit or not
    public void toggleSitMode(Player player) {
        plugin.loadPlayerStorage();
        boolean sitMode = playerSitMode.get(player);
        playerSitMode.put(player, !sitMode);
        // Save the player sit mode to storage
        plugin.getPlayerStorage().set(player.getUniqueId().toString(), !sitMode);
        plugin.savePlayerStorage();
        // invert sit mode because it was the previous value before the toggle
        if (!sitMode) {
            player.sendMessage(ChatColor.GREEN + "You can now sit in chairs!");
        } else {
            player.sendMessage(ChatColor.RED + "You can no longer sit in chairs!");
        }
    }

    public ArmorStand removePlayerArmorStandPair(Player player) {
        ArmorStand as = playerArmorStandPair.remove(player);
        if (as != null) {
            removedStandUUIDs.add(as.getUniqueId().toString());
        }
        return as;
    }

    public void setPlayerArmorStandPair(Player player, ArmorStand armorStand) {
        playerArmorStandPair.put(player, armorStand);
    }

    public Set<Player> getPlayerSet() {
        return playerArmorStandPair.keySet();
    }

    public ArmorStand getPlayerArmorStandPair(Player player) {
        return playerArmorStandPair.get(player);
    }

    public HashSet<ArmorStand> getArmorstands() {
        return new HashSet<>(playerArmorStandPair.values());
    }

    public void setPlayerLegsLocked(Player player, boolean locked) {
        playerLegsLocked.put(player, locked);
    }

    public void removePlayerLegsLocked(Player player) {
        playerLegsLocked.remove(player);
    }

    public boolean getPlayerLegsLocked(Player player) {
        return playerLegsLocked.get(player);
    }

    public boolean removePlayerSitMode(Player player) {
        if (playerSitMode.get(player) == null) {
            return true;
        }
        return playerSitMode.remove(player);
    }

    public void setPlayerSitMode(Player player, Boolean sitMode) {
        playerSitMode.put(player, sitMode);
    }

    public boolean getPlayerSitMode(Player player) {
        if (playerSitMode.get(player) == null) {
            return true;
        }
        return playerSitMode.get(player);
    }

    public HashMap<Player, Boolean> getPlayerSitMap() {
        return playerSitMode;
    }

    public Location removeLastLocation(Player player) {
        return lastLocation.remove(player);
    }

    public void addLastLocation(Player player, Location location) {
        lastLocation.put(player, location);
    }

    public HashSet<Block> getUsedBlocks() {
        return new HashSet<>(usedBlocks.values());
    }

    public void removeUsedBlock(Player player) {
        usedBlocks.remove(player);
    }

    public void addUsedBlock(Player player, Block block) {
        usedBlocks.put(player, block);
    }

}
