package com.rast.stairchairs;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class StairChairToggle implements CommandExecutor {

    private final PlayerManager playerManager;

    // Get the plugin and player manager
    public StairChairToggle (StairChairs plugin) {
        playerManager = plugin.getPlayerManager();
    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        // Must be a player
        if (sender instanceof Player) {
            Player player = (Player) sender;
            // Toggle the mode
           playerManager.toggleSitMode(player);
        } else {
            sender.sendMessage(ChatColor.RED + "You must be a player to execute this command!");
        }
        return true;
    }
}
