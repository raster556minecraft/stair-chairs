/**
 *     Stair Chairs is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     Foobar is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with Stair Chairs.  If not, see <https://www.gnu.org/licenses/>.
 */



package com.rast.stairchairs;

import org.bukkit.Bukkit;
import org.bukkit.configuration.InvalidConfigurationException;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitTask;

import java.io.*;
import java.util.*;

public class StairChairs extends JavaPlugin {

    private PlayerManager playerManager;
    private YamlConfiguration playerStorage;
    private File playerStorageFile;
    private BukkitTask legRotationCheckTaskObject;

    @Override
    public void onEnable() {

        // Setup config
        saveDefaultConfig();
        reloadConfig();

        // Setup player storage
        createPlayerStorage();

        // Setup stand storage
        //createStandStorage();

        // Create the player manager
        playerManager = new PlayerManager(this);

        // Register Events
        getServer().getPluginManager().registerEvents(new EventListener(this), this);

        // Setup commands
        Objects.requireNonNull(getCommand("StairChairToggle")).setExecutor(new StairChairToggle(this));

        // Cache player data for already existing players (When server is reloaded)
        for (Player player : Bukkit.getOnlinePlayers()) {
            playerManager.setPlayerSitMode(player, playerStorage.getBoolean(player.getUniqueId().toString(), true));
        }

        // Start the leg check tick
        legRotationCheckTick();
    }

    @Override
    public void onDisable() {
        legRotationCheckTaskObject.cancel();

        // Save the cache to the player storage
        for(Map.Entry<Player, Boolean> entry : playerManager.getPlayerSitMap().entrySet()) {
            Player player = entry.getKey();
            Boolean sitMode = entry.getValue();

            playerStorage.set(player.getUniqueId().toString(), sitMode);
        }

        // Remove all armor stands when the server quits
        for(ArmorStand as : playerManager.getArmorstands()) {
            as.remove();
        }
        savePlayerStorage();
    }

    public FileConfiguration getPlayerStorage() {
        return this.playerStorage;
    }

    public PlayerManager getPlayerManager() {
        return this.playerManager;
    }

    public void savePlayerStorage () {
        try {
            playerStorage.save(playerStorageFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void loadPlayerStorage () {
        try {
            playerStorage.load(playerStorageFile);
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
        }
    }

    private void createPlayerStorage() {
        playerStorageFile = new File(getDataFolder(), "players.yml");
        if (!playerStorageFile.exists()) {
            getLogger().warning("Making Directory: " + playerStorageFile.getAbsolutePath());
            playerStorageFile.getParentFile().mkdirs();
            saveResource("players.yml", false);
        }
        playerStorage = new YamlConfiguration();

        try {
            playerStorage.load(playerStorageFile);
        } catch (IOException | InvalidConfigurationException e) {
            e.printStackTrace();
        }
    }

    private void legRotationCheckTick() {
        legRotationCheckTaskObject = Bukkit.getScheduler().runTaskTimer(this, () -> {
            for (Player player : playerManager.getPlayerSet()) {
                ArmorStand as = playerManager.getPlayerArmorStandPair(player);
                if (as != null && !playerManager.getPlayerLegsLocked(player)) {
                    as.setRotation(player.getLocation().getYaw(), 0);
                }
            }
        }, 0, 0);
    }
}
